<?php

    require_once('Weather.php');

    $weather = new Weather();

    $orders = ['temp', 'datetime'];
    $key = array_search($_GET['sort'], $orders);
    $order = $key === false ? $orders[1] : $orders[$key];
    
    $data = $weather->getDbData($order);

    include_once('monitor_view.php');
