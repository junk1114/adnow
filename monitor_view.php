<!DOCTYPE html>
<html>
<head>
    <title>Weather monitor</title>
</head>
<body>
    <p>Max: <strong><?= $data['maxTemp'] ?></strong></p>
    <p>Min: <strong><?= $data['minTemp'] ?></strong></p>
    <p>
        Sort by:&nbsp;
        <a href="?sort=temp">temp</a>&nbsp;
        <a href="?sort=datetime">datetime</a>
    </p>
    <table border="1">
        <tr>
            <th>temp</th>
            <th>datetime</th>
        </tr>
        <?php foreach ($data['temps'] as $temp): ?>
            <tr>
                <td><?= $temp['temp'] ?></td>
                <td><?= $temp['datetime'] ?></td>
            </tr>
        <?php endforeach; ?>
    </table>
</body>
</html>