<?php

class Weather
{
    const API_KEY = '24bc1056dd34b73e563e85283fa79b1c';

    public $apiUrl  = 'http://api.openweathermap.org/data/2.5/weather';
    public $city    = 'Moscow';
    public $country = 'RU';
    public $units   = 'metric';

    private $url = '';

    public function __construct()
    {
        $this->url = $this->buildUrl();
    }

    public function getData()
    {
        $json = file_get_contents($this->url);

        if ($json === false) {
            throw new Exception('Api data error');
        }

        $object = json_decode($json);

        $data = [];
        $data['temp'] = $object->main->temp;
        $data['dateTime'] = $this->getDateTime();

        return $data;
    }

    public function storeData($data)
    {
        $pdo = $this->getPdo();

        $stmt = $pdo->prepare("INSERT INTO weather (temp, `datetime`) VALUES (?, ?)");
        $stmt->bindParam(1, $data['temp']);
        $stmt->bindParam(2, $data['dateTime']);

        $stmt->execute();
    }

    public function getDbData($order)
    {
        // Можно было бы написать вспомогательный класс для работы с БД
        // я очень давно не работал с PDO
        $pdo = $this->getPdo();

        $stmt = $pdo->query("SELECT temp, `datetime` FROM weather ORDER BY $order");

        $data = [];
        $data['temps'] = $stmt->fetchAll();

        $stmt = $pdo->query('SELECT max(temp) as temp FROM weather');
        $data['maxTemp'] = $stmt->fetch()['temp'];

        $stmt = $pdo->query('SELECT min(temp) as temp FROM weather');
        $data['minTemp'] = $stmt->fetch()['temp'];

        return $data;
    }

    private function buildUrl()
    {
        return sprintf(
            '%s?q=%s,%s&units=%s&APPID=%s',
            $this->apiUrl,
            $this->city,
            $this->country,
            $this->units,
            self::API_KEY
        );
    }

    private function getDateTime()
    {
        $dateTime = new DateTime();
        $dateTime = $dateTime->format('Y-m-d H:i:s');

        return $dateTime;
    }

    private function getPdo()
    {
        $dsn = 'mysql:host=127.0.0.1;dbname=adnow';
        $user = 'root';
        $pass = '';
        $opt = [PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC];
        $pdo = new PDO($dsn, $user, $pass, $opt);

        return $pdo;
    }
}
