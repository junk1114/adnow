<?php

    require_once('Weather.php');

    try {
	    $weather = new Weather();

	    $data = $weather->getData();
	    $weather->storeData($data);

	    return 0;
    } catch (Exception $e) {
    	// Возможно добавить запись в лог системой логирования
    	echo $e->getMessage();

    	return 1;
    }
